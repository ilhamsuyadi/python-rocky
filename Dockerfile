FROM rockylinux:8.8

WORKDIR /root/

RUN cd /tmp && \
    yum update -y && \
    yum install gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel make -y && \
    curl -o Python-3.7.11.tgz https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz && \
    tar xzf Python-3.7.11.tgz && \
    cd Python-3.7.11 && ./configure --enable-optimizations --with-ensurepip=install && make altinstall && cd && \
        ln -fs /usr/local/bin/python3.7 /usr/bin/python && \
    ln -fs /usr/local/bin/python3.7 /usr/bin/python3 && \
    rm -f Python-3.7.11.tgz && \
    rm -rf /tmp/Python-3.7.11 && \
    yum -y clean all --enablerepo='*'

ENV VIRTUAL_ENV=/opt/venv
RUN python3.7 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --upgrade pip

CMD ["bash"]